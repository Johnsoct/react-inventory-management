import React from 'react';
import PropTypes from "prop-types";

class AddFishForm extends React.Component {
	static propTypes = {
		addFish: PropTypes.func
	}
    // reference for input value instead of touching DOM (against React best-practices)
    NAME = React.createRef();
    PRICE = React.createRef();
    STATUS = React.createRef();
    DESC = React.createRef();
    IMAGE = React.createRef();
    createFish = e => {
        e.preventDefault();
        const fish = {
            name: this.NAME.value.value,
            price: parseFloat(this.PRICE.value.value),
            status: this.STATUS.value.value,
            textarea: this.DESC.value.value,
            image: this.IMAGE.value.value
        }
        this.props.addFish(fish);

        // refresh form on submit
        e.currentTarget.reset();
    }
    render() {
        return <form className="fish-edit" onSubmit={this.createFish}>
            <input name="name" type="text" placeholder="Name" ref={this.NAME} />
            <input name="price" type="text" placeholder="Price" ref={this.PRICE} />
            <select name="status" placeholder="Status" ref={this.STATUS}>
              <option value="available">Fresh!</option>
              <option value="unavailable">Sold Out!</option>
            </select>
            <textarea name="desc" placeholder="Desc" ref={this.DESC} />
            <input name="image" type="text" placeholder="Image" ref={this.IMAGE} />
            <button type="submit">+ Add Fish</button>
          </form>;
    }
}

export default AddFishForm;