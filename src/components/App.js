import React from 'react';
import PropTypes from "prop-types";
import base from '../base';
import Header from './Header';
import Inventory from './Inventory';
import Order from './Order';
import sampleFishes from '../sample-fishes';
import Fish from './Fish';

class App extends React.Component {
	static propTypes = {
		match: PropTypes.object
	}
    state = {
        fishes: {},
        order: {}
    }
    addFish = fish => {
        // Reacts set state API
        // copy existing state (avoid mutations)
        const fishes = {...this.state.fishes};
        // add new fish
        fishes[`fishes${Date.now()}`] = fish;
        // set new fishes object to state.fishes (React setState API)
        this.setState({
            fishes
        });
    }
	updateFish = (key, updatedFish) => {
		// copy of current state
		const fishes = {...this.state.fishes};
		// update copy
		fishes[key] = updatedFish;
		// update state
		this.setState({ fishes });
	}
	deleteFish = key => {
		// copy of state
		const fishes = {...this.state.fishes};
		// update copy
		fishes[key] = null;
		// update state
		this.setState({ fishes });
	}
    loadSampleFishes = () => {
        this.setState({ fishes: sampleFishes });
    }
	addToOrder = key => {
		// copy state
		const order = {...this.state.order};
		// either add to order or increment existing
		order[key] = order[key] + 1 || 1;
		// call setState and update state
		this.setState({ order });
	}
	removeFromOrder = key => {
		// copy state
		const order = {...this.state.order};
		// update state
		delete order[key];
		// set state
		this.setState({ order });
	}
	componentDidMount() {
		// get localstorage
		const localStorageRef = localStorage.getItem(this.props.match.params.storeID);
		if (localStorageRef) {
			this.setState({
				order: JSON.parse(localStorageRef)
			})
		}
		this.ref = base.syncState(`${this.props.match.params.storeID}/fishes`, {
			context: this,
			state: 'fishes'
		});
	}
	componentDidUpdate() {
		// JSON.stringify turns an object into a string to store in JSON format
		localStorage.setItem(this.props.match.params.storeID, JSON.stringify(this.state.order));
	}
	componentWillUnmount() {
		// when you leave a store, cleans up any memory issues
		base.removeBinding(this.ref);
	}
    render() {
        return (
            <div className="catch-of-the-day">
                <div className="menu">
                    <Header tagline="Fresh Fish!" />
                    <ul className="fishes">
						{Object.keys(this.state.fishes).map(key =>
							<Fish
								key={key}
								index={key}
								details={this.state.fishes[key]}
								addToOrder={this.addToOrder}
							/>
						)}
                    </ul>
                </div>
                <Order fishes={this.state.fishes} order={this.state.order} removeFromOrder={this.removeFromOrder} />
                <Inventory
					addFish={this.addFish}
					loadSampleFishes={this.loadSampleFishes}
					fishes={this.state.fishes}
					updateFish={this.updateFish}
					deleteFish={this.deleteFish}
					storeID={this.props.match.params.storeID} />
            </div>
        )
    }
}

export default App;