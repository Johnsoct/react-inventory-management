import React from 'react';
import { getFunName } from '../helpers';

class StorePicker extends React.Component {
  constructor() {
    super(); // runs StorePicker (React.Component) FIRST (required)
    // binds StorePicker instance (this) to the properties of StorePicker
    this.goToStore = this.goToStore.bind(this);
  }
  // reference for input value instead of touching DOM (against React best-practices)
  myInput = React.createRef();
  // property inside of a component
  // can forgoe the constructor function by declaring properties as arrow functions
  // becomes a property bound to an arrow function
  goToStore(event) {
    // prevent form submitting
    event.preventDefault();
    // get text from input
    const store = this.myInput.value.value; // .value.value is a React + JavaScript thing
    //change page to /store/:{storeId}
    this.props.history.push(`/store/${store}`);
  }
  render() {
    return (
      // how you return nested, properly aligned HTML (JSX)
      // follows Vue's rule of only one master element (wrapper)
      // React.Fragment acts as a wrapper instead of div ^
      // comments in JSX look like { /* comment */ }, comments must be in the wrapper
      <form className="store-selector" onSubmit={this.goToStore}>
        <h2>Please Enter A Store</h2>
        <input type="text"
               required
               placeholder="Store Name"
               defaultValue={getFunName()}
               ref={this.myInput} />
        <button type="submit">Visit Store →</button>
      </form>
    );
  }
}

export default StorePicker;