import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
	apiKey: "AIzaSyAiJmFH7HXPOk9Vi96Q9GLv0W-VhPa0HQY",
	authDomain: "catch-of-the-day-chris-johnson.firebaseapp.com",
	databaseURL: "https://catch-of-the-day-chris-johnson.firebaseio.com",
});

const base = Rebase.createClass(firebaseApp.database());

// This is a named export
export { firebaseApp };

// this is a default export
export default base;
